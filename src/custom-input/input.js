import React from 'react';

class CustomInput extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			value: ""
		};
	}

	handleChange = (e) => {
		e.preventDefault();

		this.props.callback({
			characterCount: e.target.value.length
		});
	}

	handleFocus = (e) => {
		e.preventDefault();

		this.props.callback({
			isFocused: true
		});
	}

	handleBlur = (e) => {
		e.preventDefault();

		if(!e.target.value.length){
			this.props.callback({
				isFocused: false
			});
		}
	}

	render(){
		const props = this.props;

		return (
			<input
				className={"customInput"}
				type={props.type}
				placeholder={props.placeholder}
				name={props.name}
				id={props.name}
				onChange={this.handleChange}
				onFocus={this.handleFocus}
				onBlur={this.handleBlur}
				disabled={ props.disabled ? true : null }
				maxLength={ props.characterCounterLimit }
			/>
		);
	}
}

export default CustomInput;