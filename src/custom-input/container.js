import React from 'react';
import CustomInput from './input';

class CustomInputContainer extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			characterCount: 0,
			isFocused: false,
			errorMessage: ""
		};
	}

	handleChild = (dataFromChild) => {
		this.setState(dataFromChild);
	}

	render(){
		const defaultProps = {
			type: "text",
			placeholder: "Active text",
			name: null,
			id: null,
			disabled: false,
			leadingIcon: null,
			trailingIcon: null,
			characterCounterLimit: null,
			showHelperMessage: false
		};

		const props = {
			type: this.props.type || defaultProps.type,
			placeholder: this.props.placeholder || defaultProps.placeholder,
			name: this.props.name || defaultProps.name,
			disabled: this.props.disabled || defaultProps.disabled,
			label: this.props.label || null,
			leadingIcon: this.props.leadingIcon || defaultProps.leadingIcon,
			trailingIcon: this.props.trailingIcon || defaultProps.trailingIcon,
			characterCounterLimit: this.props.characterCounterLimit || defaultProps.characterCounterLimit,
			showHelperMessage: this.props.showHelperMessage || defaultProps.showHelperMessage
		}

		return(
			<>
				<label
					className={
						this.state.isFocused ? "inputContainer focus" : "inputContainer"
					}>
					<div className={"inputField"}>
						{ props.leadingIcon &&
							<div className={"inputLeadingIcon"}>{props.leadingIcon}</div>
						}

						{ props.trailingIcon &&
							<div className={"inputTrailingIcon"}>{props.trailingIcon}</div>
						}

						<div className={"inputText"}>
							{ props.label &&
								<div className={"inputLabel"}>{props.label}</div>
							}

							<CustomInput
								callback={this.handleChild}
								{...props} />
						</div>
					</div>

					{ (props.showHelperMessage || props.characterCounterLimit) &&
						<div className={"inputHelpers"}>
							<div className={"inputHelperMessage"}>{this.state.helperMessage && this.state.helperMessage}</div>

							{ props.characterCounterLimit && 
								<div className={"inputCounter"}>{this.state.characterCount} / {props.characterCounterLimit}</div>
							}
						</div>
					}
				</label>
			</>
		);
	}
}

export default CustomInputContainer;