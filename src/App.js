import './App.scss';
import React from 'react';
import CustomInputContainer from './custom-input/container';

function App() {
  return (
		<CustomInputContainer
			characterCounterLimit={20}
			label="Label"
		/>
  );
}

export default App;